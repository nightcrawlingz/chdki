function SaveModul(){
	var kab_kota = $('#kab_kota').val();
	var id = $('#id').val();
	var id_propinsi = $('#id_propinsi').val();
	var method = $('#method').val();
	$.post("./g3n_kabkota.php", 
		{'method': method, 'id_propinsi':id_propinsi, 'kab_kota':kab_kota, 'id':id},
		function(data){ LoadGrid(data); }, 
		"json"
	);
}

function LoadData(){
	$.post("./g3n_kabkota.php", 
			{'method':'load'},
			function(data){ LoadGrid(data); }, 
			"json"
	);
}

function ResetModul(data){
	$('#id_propinsi_temp').val('0');
	$('#id').val(0);
	$('#kab_kota').val('');
	$('#method').val('input');
	get_propinsi($('#pilih_propinsi').val(), '#id_propinsi');
}

function LoadGrid(data){
	$('#table-data-body').html('');
	var strbody = '';
	var kel = '';
	var e, no;
	for ( var i in data ) {
		e = i % 2;
		no =  parseInt(i) + 1;
		strbody += "<tr id='data_row_"+ data[i].id +"' class='e"+e+"'>";
		strbody += "<td>" + no + "</td>";
		strbody += "<td>" + data[i].kab_kota +"</td>";
		strbody += "<td>" + data[i].propinsi +"</td>";
		strbody += "<td class='ctr'>";
		strbody += "<a href='#' id='edit_"+data[i].id+"' class='btn' onclick='edit_data("+ data[i].id +"); return false;' >edit</a> ";
		strbody += "<a href='#' id='delete_"+ data[i].id +"' class='btn' onclick='delete_data("+ data[i].id +");return false;' >delete</a>";
		strbody += "</td>";
		strbody += "</tr>";
	}
	$('#table-data-body').html(strbody);
}

function getby(value){
	$.post("g3n_kabkota.php", 
		{'id':value, 'method':'getby'}, 
		function(data){
			$('#id').val(data.id); 
			$('#kab_kota').val(data.kab_kota);
			get_propinsi(data.id_propinsi, '#id_propinsi');
		},
		"json"
	);
	
}

function edit_data(value){
	getby(value);
	$('#method').val('update');
	$('#InputForm').dialog('open');
}

function delete_data(value){
	if (confirm('Hapus Data, Anda Yakin ?')) { 
		$.post("g3n_kabkota.php", {'id' : value, 'method':'delete'},
			  function(data){
				$("#data_row_" + value).fadeOut('slow', function(){$(this).remove()});
			  }, "json");
	}
}

function get_propinsi(value, id_html){
	$.post("g3n_propinsi.php", {'method':'load'},
		function(data){
			var strbody = "";
			if(parseInt(value) == 0)
				strbody += "<option value='0' selected>Pilih Propinsi</option>";
			else 
				strbody += "<option value='0'>Pilih Propinsi</option>";
			var e, no;
			for(var i in data){
				no = parseInt(i) + 1;
     			if(value != data[i].id) strbody += "<option value='"+data[i].id+"'>"+data[i].propinsi+"</option>";
     			else strbody += "<option value='"+data[i].id+"' selected>"+data[i].propinsi+"</option>";
			}
			$(id_html).html(strbody);
		}, "json"
	);
}

function Startup(){
	//alert('startup');
	get_propinsi('0', '#pilih_propinsi');
	$('#pilih_propinsi').change(function(){
		//~ View_kabkota_propinsi($('#pilih_propinsi').val());
		View_kabkota_propinsi($('#pilih_propinsi').val());
	});
	
}

function View_kabkota_propinsi(value){
	//alert(value);
	$.post("./g3n_kabkota.php", 
			{'method':'load','id_propinsi':value},
			function(data){ LoadGrid(data); }, 
			"json"
	);
}

