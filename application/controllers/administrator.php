<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller {
    function __construct(){
        parent::__construct();
        #parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
        $this->load->helper(array('url','date'));
        $this->load->model(array('Select_db','Insert_db','Update_db','Delete_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
		
	$this->email_from='notif@disdik.jakarta.go.id';
	$this->email_name='Dinas Pendidikan Provinsi DKI Jakarta';
	
	$this->provinsi=31;
    }
    function analytics(){
        $this->load->view('admin/analytics_view');
    }
    
    function user_handling($param=null, $id=null){
	$this->initlib->cek_session_admin();
	if($param=='tambah'){
	    $this->load->view('admin/user_handling_tambah_view');
	}
	if($param=='edit'){
	    $data['user']=$this->Select_db->user_handling(array('id' => $id))->row();
	    $this->load->view('admin/user_handling_tambah_view',$data);
	}
	else{
	    $this->load->view('admin/user_handling_view');    
	}
	
    }
    
    function post_user_handling($id=null){
	if($id){
	    //update
	    $data_user=array(
		'userid' => $this->input->post('userid'),
		'username' => $this->input->post('username'),
		'activated' => $this->input->post('activated'),
		'level_id' => $this->input->post('level'),
		'kabupaten' => ($this->input->post('level')==2 ? $this->input->post('kabkota') : null),
		'propinsi' => ($this->input->post('level')==1 ? $this->provinsi : null)
	    );
	    
	    if($this->input->post('password')!=''){
		$data_user['password']= $this->input->post('password');
	    }
	    $role=$this->input->post('role');
	    $this->Update_db->user_handling($id,$data_user,$role);
	    
	}else{
	    //insert
	    
	    $data_user=array(
		'userid' => $this->input->post('userid'),
		'username' => $this->input->post('username'),
		'password' => md5($this->input->post('password')),
		'activated' => $this->input->post('activated'),
		'level_id' => $this->input->post('level'),
		'kabupaten' => ($this->input->post('level')==2 ? $this->input->post('kabkota') : null),
		'propinsi' => ($this->input->post('level')==1 ? $this->provinsi : null)
	    );
	    
	    
	    $role=$this->input->post('role');
	    $this->Insert_db->user_handling($data_user,$role);
	    
	}
	redirect('administrator/user_handling');
    }
    function ajax_user_handling(){

	$this->datatables
	->select("null as no, 
	    a.userid, 
	    a.username, 
	    d.name as level,
	    if(d.id = 1, e.NmProv, if(d.id = 2, f.NmKab, null)) as daerah, 
	    GROUP_CONCAT(c.nama SEPARATOR ',') as role,
	    if(activated=1,'ya','tidak') as activated,
	    a.id as user_id",false
	)
	->from('t_user a')
	->join('pol_role_jenjang b','b.user_id = a.id','left')
	->join('g3n_jenjang_sekolah c',' c.id = b.jenjang_id','left')
	->join('pol_level d','d.id = a.level_id','left')
	->join('g3n_propinsi e','e.KdProv = a.propinsi','left')
	->join('g3n_kabupaten f','f.KdKab = a.kabupaten','left')
	->where('a.deleted = 0')
	->group_by('a.id','asc')
	->edit_column('user_id','<a href='.site_url('administrator/user_handling/edit').'/$1>Edit</a> | <a href=#>Delete</a>','user_id');
	echo $this->datatables->generate();
    }
    
    function form_delete_ponline($param,$id){
				$url_post='';
				if($this->uri->segment(3)=='ponline'){
						$url_post=site_url('administrator/post_table/delete/'.$id);
				}elseif($this->uri->segment(3)=='psms'){
						$url_post=site_url('administrator/post_table_sms/delete/'.$id);
				}
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <title>New Item</title>
                </head>
                <body>
                <div>
                <form action="'.$url_post.'" method="post">
                   <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Alasan:<br><textarea name="alasan"></textarea></p>
                   <p><input type="submit" name="submit" value="Add Task" /></p>
                </form>
                </div>
                </body>
                
                </html>';
    }
    function ajax_ponline(){
        
        $this->datatables
        ->select('pol_ponline.id,
                IF(g3n_pengaduan.KdPengaduan is null,"belum","terima") as status,
                pol_ponline.TglKetahui as tanggal,
                g3n_kategori.short as kategori,
                pol_ponline.KdPengaduan as kode_pengaduan,
                pol_ponline.Deskripsi as deskripsi,
                pol_ponline.NmSekolah as sekolah ,
                CONCAT_WS(", ",g3n_propinsi.NmProv,g3n_kabupaten.NmKab,g3n_kecamatan.NmKec) as lokasi,
                IF(g3n_pengaduan.KdPengaduan is null,CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/index/view').'/",pol_ponline.id,"\">Lihat</a> | <a class=\"confirm_terima\" href=\"#\" data-id=\"",pol_ponline.id,"\">Terima</a> | <a title=\"Tolak\" class=\"dialog-form\" href=\"'.site_url('administrator/form_delete_ponline/ponline').'/",pol_ponline.id,"\">Tolak</a>"),CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/index/view').'/",pol_ponline.id,"\">Lihat</a>")) 
                ',false)
        ->from('g3n_kategori, g3n_sumber_info, g3n_propinsi, g3n_kabupaten, g3n_kecamatan, pol_ponline')
        ->join('g3n_pengaduan','g3n_pengaduan.KdPengaduan = pol_ponline.KdPengaduan and g3n_pengaduan.media=1','left')
        ->where('pol_ponline.KdProv = g3n_propinsi.KdProv')
        ->where('pol_ponline.KdKab = g3n_kabupaten.KdKab')
	->where('pol_ponline.KdKec = g3n_kecamatan.KdKec')
	->where('pol_ponline.KdKategori = g3n_kategori.KdKat')
	->where('pol_ponline.KdSumber = g3n_sumber_info.KdSumber')
        ->where('pol_ponline.is_delete','0')
	//->add_column('delete','<a  class="iframe" href="'.site_url('administrator/index/view').'/$1" >Lihat</a>','pol_ponline.id')
	->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")')
        ->edit_column('deskripsi','<div class="expandable"><p>$1</p></div>','deskripsi');
        //->edit_column('null','<input value="$1" type="checkbox" name="rowid[]"/>','pol_ponline.id');
        //mysqldatetime_to_date($row->tanggal, $format = "d/m/Y, H:i:s")
        echo $this->datatables->generate();
    }
	
	function ajax_psms(){
        
        $this->datatables
        ->select('pol_psms.id,
						IF(g3n_pengaduan.KdPengaduan is null,"belum","terima") as status,
						pol_psms.time as tanggal,
						g3n_kategori.short as kategori,
						pol_psms.KdPengaduan as kode_pengaduan,
						pol_psms.pengaduan as deskripsi,
						IF(g3n_pengaduan.KdPengaduan is null,pol_psms.sekolah,g3n_pengaduan.NmSekolah) as sekolah ,
						IF(g3n_pengaduan.KdPengaduan is null,pol_psms.lokasi,CONCAT_WS(", ",g3n_propinsi.NmProv,g3n_kabupaten.NmKab,g3n_kecamatan.NmKec)) as lokasi,
						IF(g3n_pengaduan.KdPengaduan is null,CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/index/view_sms').'/",pol_psms.id,"\">Edit</a> | <a title=\"Tolak\" class=\"dialog-form\" href=\"'.site_url('administrator/form_delete_ponline/psms').'/",pol_psms.id,"\">Tolak</a>"),CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/view_sms_approve').'/",pol_psms.id,"\">Lihat</a>")) 
            ',false)
        ->from('pol_psms')
				->join('g3n_pengaduan','pol_psms.KdPengaduan = g3n_pengaduan.KdPengaduan and g3n_pengaduan.media=2','left')
				->join('g3n_propinsi','g3n_pengaduan.KdProv = g3n_propinsi.KdProv','left')
				->join('g3n_kabupaten','g3n_pengaduan.KdKab = g3n_kabupaten.KdKab','left')
				->join('g3n_kecamatan','g3n_pengaduan.KdKec = g3n_kecamatan.KdKec','left')
				->join('g3n_kategori','g3n_pengaduan.KdKategori = g3n_kategori.KdKat','left')
        
        ->where('pol_psms.is_delete','0')
				->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")')
        ->edit_column('deskripsi','<div class="expandable"><p>$1</p></div>','deskripsi');
        echo $this->datatables->generate();
				
    }
		
		function view_sms_approve($id){
				$data['prov']=$this->Select_db->prov();
				$data['sumber_info']=$this->Select_db->sumber_info();
				$data['psms']=$this->Select_db->pengaduan_sms(array('id' => $id))->row();
				//print_r($data['psms']);
				$data['id']=$id;
				$this->load->view('admin/psms_view',$data);
				
		}
    function index($param=null,$id=null){
        $this->initlib->cek_session_admin();
        if($param=='view'){
            
            $data['row']=$this->Select_db->ponline(array('id' => $id))->row();
            //print_r($id);
            $this->load->view('admin/ponline_view',$data);
        }elseif($param=='view_sms'){
						$data['prov']=$this->Select_db->prov();
						$data['sumber_info']=$this->Select_db->sumber_info();
						$data['psms']=$this->Select_db->psms(array('id' => $id))->row();
						$data['id']=$id;
						$this->load->view('home/smspengaduan_view',$data);
				}
        else{
            $this->load->view('admin/home_view');    
        }
        
    }
    function sms($param=null,$id=null){
        $this->initlib->cek_session_admin();
        if($param=='view'){
            
            $data['row']=$this->Select_db->ponline(array('id' => $id))->row();
            $data['id']=$id;
            $this->load->view('admin/ponline_view',$data);
        }
        else{
            /*
            $this->load->library('pagination_admin');
            $config['base_url'] = site_url('administrator/index');
            $config['total_rows'] = $this->Select_db->ponline()->num_rows();
            $config['per_page'] = $this->session->userdata('num_rows');
            $data['per_page'] = $config['per_page'];
            $config['uri_segment'] = '3';
            
            $data_ponline=array(
                            'offset' => $config['per_page'],
                            'limit' => $this->uri->segment($config['uri_segment'])
                        );
            $data['ponline']=$this->Select_db->ponline($data_ponline,true);
            $data['num_rows']=$config['total_rows'];
            //config link
            
            $config['first_link'] = '';
            
            
            $config['last_link'] = '';
    
            
            $config['next_link'] = '';
    
            
            $config['prev_link'] = '';
    
    
            //end config link
            $this->pagination_admin->initialize($config);
						*/
            $this->load->view('admin/sms_view',$data);    
        }
        
    }
    function login(){
        $this->load->view('admin/login_view');
    }
    
    function post_login(){
        $data['username']= $this->input->post('username');
        $data['password']= md5($this->input->post('password'));
        
        if($data=$this->Select_db->login_admin($data)){
            $newdata = array(
                'id' => $data['id'],
                'nama' => $data['nama'],
                'email' => $data['email'],
                'admin_logged_in' => TRUE,
                'num_rows' => '10'
            );
        
        $this->session->set_userdata($newdata);
        redirect('administrator');
        }else{
           redirect('administrator/login');
        }
    }
    function post_table($param=null){
        $this->initlib->cek_session_admin();
        
        
        if($param=="terima"){
            $id=$this->uri->segment(4);
            
            if($this->Insert_db->pengaduan_approve('terima',array('id' => $id))){
		$data['row']=$this->Select_db->ponline(array('id' => $id))->row();
		$data['publikasi']=true;
		$msg=$this->load->view('admin/send_email_approve',$data,true);
		$data_email=array(
		  'from' => $this->email_from.'|'.$this->email_name,
		  'to' => $data['row']->email,
		  'subject' => 'P3M disdik DKI Jakarta : Pemberitahuan status Pengaduan',
		  'body' => $msg
		);
		$this->Insert_db->send_email($data_email);
	    }
	    				
            redirect('administrator');
        }elseif($param=='terima_hide'){
	    $id=$this->uri->segment(4);
            
	    $this->db->where('id',$id);
	    $this->db->update('pol_ponline',array('published' => 0));
	    
            if($this->Insert_db->pengaduan_approve('terima',array('id' => $id))){
		$data['row']=$this->Select_db->ponline(array('id' => $id))->row();
		$data['publikasi']=false;
		$msg=$this->load->view('admin/send_email_approve',$data,true);
		$data_email=array(
		  'from' => $this->email_from.'|'.$this->email_name,
		  'to' => $data['row']->email,
		  'subject' => 'P3M disdik DKI Jakarta : Pemberitahuan status Pengaduan',
		  'body' => $msg
		);
		$this->Insert_db->send_email($data_email);
	    }			
            redirect('administrator');
						
            redirect('administrator');
	}elseif($param=='delete'){
            $id=$this->uri->segment(4);
	    $data['row']=$this->Select_db->ponline(array('id' => $id))->row();
	    $data['row']->reason_delete=$this->input->post('alasan');
	    $param=array('id' => $id, 'alasan' => $this->input->post('alasan'));
            
            if($this->Delete_db->ponline($param)){
						
                //send email
			  $msg=$this->load->view('admin/send_email_decline',$data,true);				
			  //$this->email->set_newline("\r\n");
			  /*
			  $this->email->from('bos@kemdikbud.go.id', 'BOS kemdikbud');
			  $this->email->to($data['row']->email);
			  $this->email->subject('Pemberitahuan status Pengaduan P3M BOS');
			  
			  $msg=$this->load->view('admin/send_email_decline',$data,true);
			  $this->email->message($msg);
			  
			  $this->email->send();
			  */
			  //end send email
			  
			  $data_email=array(
			    'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
			    'to' => $data['row']->email,
			    'subject' => 'Pemberitahuan status Pengaduan P3M BOS',
			    'body' => $msg
			  );
			  $this->Insert_db->send_email($data_email);
			  echo 'sukses';
			  
	  }else
                echo 'gagal';
            //redirect('administrator');
        }elseif($this->input->post('list_action')){
            $param=$this->input->post('list_action');
            $rowid=$this->input->post('rowid');
            foreach($rowid as $id){
                if($param=="terima")
                    $this->Insert_db->pengaduan_approve('terima',array('id' => $id));
                elseif($param=="delete")
                    $this->Delete_db->ponline($id);
            }
            //print_r($this->input->post('rowid'));
            redirect('administrator');
        }
        
        
    }
		function post_table_sms($param=null){
        $this->initlib->cek_session_admin();
        if($param=='delete'){
            $param=array('id' => $this->uri->segment(4), 'alasan' => $this->input->post('alasan'));
            //$id=$this->uri->segment(4);
            if($this->Delete_db->psms($param))
                echo 'sukses';
            else
                echo 'gagal';
            //redirect('administrator');
        }elseif($this->input->post('list_action')){
            $param=$this->input->post('list_action');
            $rowid=$this->input->post('rowid');
            foreach($rowid as $id){
                if($param=="terima")
                    $this->Insert_db->pengaduan_approve('terima',array('id' => $id));
                elseif($param=="delete")
                    $this->Delete_db->ponline($id);
            }
            //print_r($this->input->post('rowid'));
            redirect('administrator/sms');
        }
        
        
    }
    function post_num_rows(){
        $this->initlib->cek_session_admin();
        $this->session->set_userdata(array('num_rows' => $this->input->post('num_rows')));
        $url=$this->input->post('url');
        redirect($url);
    }
    function logout(){
		$data = array(
		'id' => $this->session->userdata('id'),
		'nama' => $this->session->userdata('nama'),
		'email' => $this->session->userdata('email'),
		'admin_logged_in' => $this->session->userdata('admin_logged_in'),
                'num_rows' => '10'
	    );
        $this->session->unset_userdata($data);
        redirect('administrator/login');
    }
    
    function faq($param=null){
        $this->initlib->cek_session_admin();
        //$data['faq']=$this->Select_db->faq()->row();
		$data['faq_kategori']=$this->Select_db->faq_kategori();
        $this->load->view('admin/faq_view',$data);
    }
    function post_faq(){
        $this->initlib->cek_session_admin();
        //$data['isi']=$this->input->post('isi');
		$kategori=$this->input->post('faq_kategori');
		$tanya=$this->input->post('tanya');
		$jawab=$this->input->post('jawab');
        $id_faq=$this->input->post('id_faq');
		if($kategori=="tambah"){
			$id_kategori=$this->Insert_db->faq_kategori(array('faq_kategori' => $this->input->post('tambah_faq_kat')));
		}else{
			$id_kategori=$kategori;
		}
		$i=0;
		foreach($tanya as $row_tanya){
			
			//echo $row_tanya.' : '.$jawab[$i];
			$data=array(
				'tanya' => $row_tanya,
				'jawab' => $jawab[$i],
				'id_faq_kategori' => $id_kategori
			);
			if($id_faq[$i]!='')
				$this->Update_db->faq($id_faq[$i],$data);
			else
				$this->Insert_db->faq($data);
			$i++;
		}
		//$this->Update_db->faq($data);
        redirect('administrator/faq/edit');
    }
	function post_faq_kategori(){
		$nama_kat=$this->input->post('faq_kat_edit');
		$id_kat=$this->input->post('id_faq_kat');
		
		$this->Update_db->faq_kategori($id_kat, array('faq_kategori' => $nama_kat));
		
		redirect('administrator/faq/edit');
	}
    
    function berita($param='list',$id=null){
        $this->initlib->cek_session_admin();
        
        if($param=='list'){
            $this->load->library('pagination_admin');
            $config['base_url'] = site_url('administrator/berita/list');
            $config['total_rows'] = $this->Select_db->berita()->num_rows();
            $config['per_page'] = $this->session->userdata('num_rows');
            $data['per_page'] = $config['per_page'];
            $config['uri_segment'] = '4';
            
            $data_berita=array(
                            'offset' => $config['per_page'],
                            'limit' => $this->uri->segment($config['uri_segment'])
                        );
            $data['berita']=$this->Select_db->berita($data_berita,true);
            $data['num_rows']=$config['total_rows'];
            //config link
            
            $config['first_link'] = '';
            
            
            $config['last_link'] = '';
    
            
            $config['next_link'] = '';
    
            
            $config['prev_link'] = '';
    
    
            //end config link
            $this->pagination_admin->initialize($config);
            $this->load->view('admin/berita_view',$data);    
        }elseif($param=='tambah'){
            $this->load->view('admin/berita_tambah_view');
        }elseif($param=='view'){
            $data['id']=$id;
            $data['berita']=$this->Select_db->berita($data['id'])->row();
            $this->load->view('admin/berita_view_view',$data);
        }elseif($param=='edit'){
            $data['id']=$id;
            $data['berita']=$this->Select_db->berita($data['id'])->row();
            
            $this->load->view('admin/berita_tambah_view',$data);
        }
        
    }
    
    function post_berita($param,$id=null){
        $this->initlib->cek_session_admin();
        
        $data['tanggal']=date_to_mysqldatetime(now());
        $data['tampil']=($this->input->post('tampil')=='' ? 'tidak' : $this->input->post('tampil'));
        $data['ringkasan']=$this->input->post('ringkasan');
        $data['isi']=$this->input->post('isi');
        $data['judul']=$this->input->post('judul');
        $data['author']=$this->session->userdata('id');
        if($param=="tambah"){
            //print_r($data);
            $this->Insert_db->berita($data);
        }elseif($param=="edit"){
            
            $this->Update_db->berita($id,$data);
        }elseif($param=="delete"){
            $this->Delete_db->berita($id);
        }
        redirect('administrator/berita');
    }
    function user(){
        $this->initlib->cek_session_admin();
        $data['user']=$this->Select_db->user()->row();
        $this->load->view('admin/user_view',$data);
    }
    function post_user($param,$id){
        $this->initlib->cek_session_admin();
        
        if($param=='edit'){
            $data['nama']=$this->input->post('nama');
            $data['username']=$this->input->post('username');
						$data['email']=$this->input->post('email');
						$data['notifikasi']=$this->input->post('notifikasi');
            $this->Update_db->user($id,$data);
        }
        redirect('administrator/user');
    }
    function password(){
        $this->initlib->cek_session_admin();
        $data['user']=$this->Select_db->user()->row();
        $this->load->view('admin/password_view',$data);
    }
    function post_password($param,$id){
        $this->initlib->cek_session_admin();
        if($param=='edit'){
            $user=$this->Select_db->user()->row();
            if(md5($this->input->post('password_lama'))== $user->password){
                if($this->input->post('password_baru') == $this->input->post('password_baru1')){
                    $data['password']=md5($this->input->post('password_baru'));
                    $this->Update_db->user($id,$data);
                }    
            }
            
        }
        redirect('administrator/password');
    }
		function post_pengaduan_sms(){
				$id=$this->input->post('id');
				$data['KdProv']=$this->input->post('KdProv');
				$data['KdKab']=$this->input->post('KdKab');
				$data['KdKec']=$this->input->post('KdKec');
				//$data['KdDesa']=$this->input->post('KdDesa');
				$data['KdJenjang']=$this->input->post('KdJenjang');
				$data['NmSekolah']=$this->input->post('NmSekolah');
				$data['KdKategori']=$this->input->post('KdKategori');
				$data['KetRpKat4']=$this->input->post('KetRpKat4');
				//$data['KdSumber']=$this->input->post('KdSumber');
				//$data['LainSumber']=$this->input->post('LainSumber');
				$data['Deskripsi']=$this->input->post('Deskripsi');
				
				$psms['msisdn']=$this->input->post('telp');
				$psms['alamat']=$this->input->post('alamat');
				$data['TglKetahui']=$this->input->post('tanggal');
				$data['media']=2; //kode pengaduan sms
				$data['KdPengaduan']=$this->initlib->kodePengaduan($data['KdJenjang'], $data['KdKategori'], $data['KdKab'],mysqldatetime_to_date($data['TglKetahui'],'d-m-Y'));
				$psms['KdPengaduan']=$data['KdPengaduan'];
				
				//update psms dan insert g3n_pengaduan
				$this->Insert_db->pengaduan_approve_sms($id,$data,$psms);
				
				$this->output->set_output('
				<script type="text/javascript" src="'.$this->config->item('home_js').'/jquery-1.7.1.min.js"></script>
				<script>
					$(document).ready(function() {
						parent.reload_page();
						parent.jQuery.fancybox.close();
						
				  })
				</script>
				');
				
				//print_r($data);
				//print_r($psms);
		}
		
		function send_email_pengaduan_dki(){
		  //echo getmypid();
		  //die();
		  $count_run=intval(shell_exec("ps -ef | grep -v grep | grep send_email_pengaduan_dki | wc -l 2>&1"));
		  $via=php_sapi_name();
		  echo "------------------------------\n";
		  echo date("Y-m-d H:i:s")." via={$via} count_run={$count_run}\n";
		  while(1){
		    $count_run=intval(shell_exec("ps -ef | grep -v grep | grep send_email_pengaduan_dki | wc -l 2>&1"));
		    if(php_sapi_name() == "cli" && $count_run <= 2) {//hasil trace ternyata di crontab muncul 2 proses
		      echo "run process\n";		    
		      echo "run looping process\n";
		      $send_email=$this->Select_db->send_email();
		      foreach($send_email->result() as $row){
			if(filter_var($row->to, FILTER_VALIDATE_EMAIL)){
			  $from=explode('|',$row->from);
			  $this->email->from($from[0], $from[1]);
			  $this->email->to($row->to);
			  $this->email->cc($row->cc);
			  $this->email->subject($row->subject);
			  $this->email->message($row->body);
			  if($this->email->send()){
			    echo "---------------------\n";
			    echo $row->to."\n";
			    echo "---------------------\n";
			    $this->Update_db->send_email($row->id,array('sent' => '1'));
			    $this->Update_db->send_email($row->id,array('executed' => '1'));
			  }
			}
		      }
		    }else{
		      $mypid=getmypid();
		      echo "die ".date("Y-m-d H:i:s")."\n";
		      echo "------------------------------\n";
		      die();
		      shell_exec("ps -9 {$mypid}");
		    }
		      
		    sleep(1*60);
		  }
		    
		  
		  echo "closed ".date("Y-m-d H:i:s")."\n";
		  echo "------------------------------\n";
		  die();
		}
		
		function notif_email(){
				
				$this->config->set_item('base_url','http://bos.kemdikbud.go.id/dki') ;
				$this->config->set_item('home_img',site_url("/media/home/images")) ;
				
				if(php_sapi_name() == "cli") {
						
						$data['user']=$this->Select_db->user()->row();
						
						//pengaduan sms
						$this->db
						->select('pol_psms.id,
								IF(g3n_pengaduan.KdPengaduan is null,"belum","terima") as status,
								pol_psms.time as tanggal,
								pol_psms.msisdn,
								g3n_kategori.short as kategori,
								pol_psms.KdPengaduan as kode_pengaduan,
								pol_psms.pengaduan as deskripsi,
								IF(g3n_pengaduan.KdPengaduan is null,pol_psms.sekolah,g3n_pengaduan.NmSekolah) as sekolah ,
								IF(g3n_pengaduan.KdPengaduan is null,pol_psms.lokasi,CONCAT_WS(", ",g3n_propinsi.NmProv,g3n_kabupaten.NmKab,g3n_kecamatan.NmKec)) as lokasi,
								IF(g3n_pengaduan.KdPengaduan is null,CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/index/view_sms').'/",pol_psms.id,"\">Edit</a> | <a title=\"Tolak\" class=\"dialog-form\" href=\"'.site_url('administrator/form_delete_ponline/psms').'/",pol_psms.id,"\">Tolak</a>"),CONCAT("<a class=\"iframe\" href=\"'.site_url('administrator/view_sms_approve').'/",pol_psms.id,"\">Lihat</a>")) 
								',false)
						->from('pol_psms')
						->join('g3n_pengaduan','pol_psms.KdPengaduan = g3n_pengaduan.KdPengaduan and g3n_pengaduan.media=2','left')
						->join('g3n_propinsi','g3n_pengaduan.KdProv = g3n_propinsi.KdProv','left')
						->join('g3n_kabupaten','g3n_pengaduan.KdKab = g3n_kabupaten.KdKab','left')
						->join('g3n_kecamatan','g3n_pengaduan.KdKec = g3n_kecamatan.KdKec','left')
						->join('g3n_kategori','g3n_pengaduan.KdKategori = g3n_kategori.KdKat','left')
						
						->where('g3n_pengaduan.KdPengaduan is null')
						->where('pol_psms.is_delete','0');
						
						$data['not_approve_sms']=$this->db->get();
				
						//pengaduan online
						$data['not_approve']=$this->Select_db->ponline(array('not_approve'=>true));
						if($data['user']->notifikasi=='1'){
								echo "---------------------\n";
								echo date("d-m-Y H:i:s")."\n";
								if($data['not_approve']->num_rows()!=0 || $data['not_approve_sms']->num_rows()!=0){
										
										//$this->email->set_newline("\r\n");
										$this->email->from('notif@hdisdik.jakarta.go.id', 'disdik DKI Jakarta');
										$this->email->to($data['user']->email);
										$this->email->subject('Pemberitahuan '.($data['not_approve']->num_rows()+$data['not_approve_sms']->num_rows()).' Pengaduan yang masuk P3M BOP-KJP');
										
										$msg=$this->load->view('admin/notif_email',$data,true);
										$this->email->message($msg);
										
										if($this->email->send())
														echo "terkirim ke ".$data['user']->email;
																//$this->session->set_flashdata('valid',true);
												else
														echo "gagal kirim ke ".$data['user']->email;
																//$this->session->set_flashdata('valid',false);
										
										
								}else
										echo "semuanya sudah di verifikasi..\n";
								
								echo "\n";
								echo "---------------------\n";
						}
				}
				
		}
		function notif_email_aplikasi(){
				$this->config->set_item('base_url','http://bos.kemdikbud.go.id/pengaduan') ;
				$this->config->set_item('home_img',site_url("/media/home/images")) ;
				
				$user=$this->Select_db->t_user('aktif');
				foreach($user->result() as $row){
						
						$data['instansi']=strtolower($row->instansi);
						$data['nama']=$row->username;
						if(strtolower($row->instansi) == 'pusat'){
								$data['notif']=$this->Select_db->notif_email_aplikasi('pusat',array('id_jenjang' => $row->jenjang));
								
								$data['total']=0;
								foreach($data['notif']->result() as $notif)
									$data['total']+=$notif->jumlah;
								
								
						}elseif(strtolower($row->instansi) == 'propinsi'){
								$id_prov=substr($row->userid,4);
								
								$data['notif']=$this->Select_db->notif_email_aplikasi('propinsi',array('id_jenjang' => $row->jenjang, 'id_prov' => $id_prov));
								
								$data['total']=0;
								foreach($data['notif']->result() as $notif)
									$data['total']+=$notif->jumlah;
								
								
						}elseif(strtolower($row->instansi) == 'daerah'){
								$id_kabkota=substr($row->userid,3);
								$data['notif']=$this->Select_db->notif_email_aplikasi('daerah',array('id_jenjang' => $row->jenjang, 'id_kabkota' => $id_kabkota));
								
								$data['total']=0;
								foreach($data['notif']->result() as $notif)
									$data['total']+=$notif->jumlah;
								
								
						}
						
						if($data['total']>0){
								if($row->notifikasi==1)
										if(php_sapi_name() == "cli") {
												//cli menggunakan daemon
												
												//kirim email
												echo "---------------------\n";
												echo date("d-m-Y H:i:s")."\n";
												
												$this->email->from('bos@kemdikbud.go.id', 'BOS kemdikbud');
												$this->email->to($row->email);
												//$this->email->to('panji09@gmail.com'); //testing
												$this->email->subject('Pemberitahuan '.$data['total'].' Pengaduan yang belum ditanggapi P3M BOS');
												
												$msg=$this->load->view('admin/notif_email_aplikasi',$data,true);
												$this->email->message($msg);
												
												if($this->email->send())
														echo $row->username." terkirim ke ".$row->email;
																//$this->session->set_flashdata('valid',true);
												else
														echo $row->username." gagal kirim ke ".$row->email;
																//$this->session->set_flashdata('valid',false);
												
												echo "\n";
												echo "---------------------\n";
										}else{
												//non cli
												$this->load->view('admin/notif_email_aplikasi',$data);
										}
								else
										echo $row->username." (".$row->email.") notifikasi false\n";
						}else
								echo $row->username." pengaduan kosong\n";
						
						
						//echo $row->username.' '.$row->email.'<br>';
						
				}
				/*
				$q=$this->Select_db->notif_email_aplikasi('pusat',array('id_jenjang' => 3));
				print_r($q->result());
				*/
		}
	
		function fix_kode_pengaduan(){
				//g3n_pengaduan
				echo 'fix g3n_pengaduan<br>';
				$this->db->like('KdPengaduan','e');
				$out=$this->db->get('g3n_pengaduan');
				//print_r($out);
				
				$i=1;
				foreach($out->result() as $row){
						echo $i.' '.$row->KdPengaduan.'<br>';
						$i++;
						
						$this->db->where('KdPengaduan',$row->KdPengaduan);
						$this->db->update('g3n_pengaduan',array('isupdate' => '1', 'KdPengaduan' => substr($row->KdPengaduan, 0,-1)));
				}
				
				//pol_psms
				echo 'fix pol_psms<br>';
				$this->db->like('KdPengaduan','e');
				$out=$this->db->get('pol_psms');
				//print_r($out);
				
				$i=1;
				foreach($out->result() as $row){
						echo $i.' '.$row->KdPengaduan.'<br>';
						$i++;
						
						$this->db->where('KdPengaduan',$row->KdPengaduan);
						$this->db->update('pol_psms',array('KdPengaduan' => substr($row->KdPengaduan, 0,-1)));
				}
				//pol_ponline
				echo 'fix pol_ponline<br>';
				$this->db->like('KdPengaduan','e');
				$out=$this->db->get('pol_ponline');
				//print_r($out);
				
				$i=1;
				foreach($out->result() as $row){
						echo $i.' '.$row->KdPengaduan.'<br>';
						$i++;
						
						$this->db->where('KdPengaduan',$row->KdPengaduan);
						$this->db->update('pol_ponline',array('KdPengaduan' => substr($row->KdPengaduan, 0,-1)));
				}
				
				//g3n_update
				echo 'fix g3n_update<br>';
				$this->db->like('KdPengaduan','e');
				$out=$this->db->get('g3n_update');
				//print_r($out);
				
				$i=1;
				foreach($out->result() as $row){
						echo $i.' '.$row->KdPengaduan.'<br>';
						$i++;
						
						$this->db->where('KdPengaduan',$row->KdPengaduan);
						$this->db->update('g3n_update',array('KdPengaduan' => substr($row->KdPengaduan, 0,-1)));
				}
				
		}
		
		function test_count(){
		  $count_run=intval(shell_exec("ps -ef | grep -v grep | grep test_count | wc -l"));
		  echo $count_run;
		}
		function fix_pelaku(){
				/*
				$this->db->where('pKomite = "null"');
				$this->db->or_where('pKomite is NULL');
				$this->db->or_where('pKomite = "false"');
				
				$this->db->or_where('pGuru = "null"');
				$this->db->or_where('pGuru is NULL');
				$this->db->or_where('pGuru = "false"');
				
				$this->db->or_where('pLain = "null"');
				$this->db->or_where('pLain is NULL');
				$this->db->or_where('pLain = "false"');
				
				$this->db->or_where('pBendSek = "null"');
				$this->db->or_where('pBendSek is NULL');
				$this->db->or_where('pBendSek = "false"');
				
				$this->db->or_where('pLSM = "null"');
				$this->db->or_where('pLSM is NULL');
				$this->db->or_where('pLSM = "false"');
				
				$this->db->or_where('pDinas = "null"');
				$this->db->or_where('pDinas is NULL');
				$this->db->or_where('pDinas = "false"');
				
				$this->db->or_where('pUPTD = "null"');
				$this->db->or_where('pUPTD is NULL');
				$this->db->or_where('pUPTD = "false"');
				
				$update=array(
						'pKomite' => 'FALSE',
						'pGuru' => 'FALSE',
						'pBendSek' => 'FALSE',
						'pLSM' => 'FALSE',
						'pDinas' => 'FALSE',
						'pUPTD' => 'FALSE',
						'pLain' => 'FALSE'
						
				);
				$this->db->update('g3n_pengaduan',$update);
				
				*/
				//$this->db->update('g3n_pengaduan',array('pGuru' => 'false'));
				
				
		}
		
}

/* End of file administrator.php */
/* Location: ./application/controllers/administrator.php */
?>