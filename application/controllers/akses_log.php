<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Akses_log extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('Select_db','Insert_db','Update_db','Delete_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
    }
    
    function index(){
        $this->initlib->cek_session_handling();
        $filter=$this->session->userdata('filter_akses_log');
        
        $data['title'] = 'Aplikasi P3M : Akses Log';
        $data['module']='mod_akses_log';
        
        $data_param=array();
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                if(isset($filter['provinsi'])){
                    $data_param['id_prov']=$filter['provinsi'];
                }
            break;
            
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
        }
        
        $data['akses_log']=$this->Select_db->akses_login($data_param);
        //echo $this->db->last_query();
        $this->load->view('handling/main_view',$data);
    }
    
    function post_akses_log(){
        if($this->input->post('provinsi')==''){
            $this->session->unset_userdata('filter_akses_log');    
        }else{
            $data['provinsi']=$this->input->post('provinsi');
            $this->session->set_userdata('filter_akses_log',$data);    
        }
        
        redirect('akses_log');
    }
}