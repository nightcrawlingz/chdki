<div id="dialog-ui-logout" title="Konfirmasi">
  <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Apakah Anda Ingin Keluar?
</div>
<div id="dialog-ui-kat_faq_edit" title="Edit Kategori FAQ">
  <span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
  <form method="post" action="<?=site_url('administrator/post_faq_kategori')?>" id="form_kat_faq_edit">
  	<table>
		<tr>
			<td>FAQ Kategori</td>
			<td>:</td>
			<td><input name="faq_kat_edit" id="faq_kat_edit" type="text" value="" /> <input name="id_faq_kat" type="hidden" id="id_faq_kat" value="" /></td>
		</tr>
	</table>
  </form>
</div>

<div id="dialog-ui-confirm_publish" title="Konfirmasi">
  <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Apakah akan ditampilkan pengaduan ini?
</div>

<div id="dialog-ui-kat_faq_hapus" title="Hapus Kategori FAQ">
	<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="pesan_hapus"></div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#dialog-ui-logout").dialog({
		autoOpen: false,
		modal: true
	});
	$(".confirmLink-logout").click(function(e) {
		e.preventDefault();
		var targetUrl = $(this).attr("href");
		
		$("#dialog-ui-logout").dialog({
			buttons : {
				"Ya" : function() {
				  window.location.href = targetUrl;
				},
				"Tidak" : function() {
				  $(this).dialog("close");
				}
			}
		});
		$("#dialog-ui-logout").dialog("open");
	});
	
	$("#dialog-ui-confirm_publish").dialog({
		autoOpen: false,
		modal: true
	});
	
	
	
	//edit kategori faq
	$("#dialog-ui-kat_faq_edit").dialog({
		autoOpen: false,
		modal: true,
		width: 'auto'
	});
	
	$("#confirmLink-kat_faq_edit").click(function(e) {
		faq_kat=$("#faq_kategori option:selected").html();
		id_faq_kat=$("#faq_kategori").val();
		$("#faq_kat_edit").val(faq_kat);
		$("#id_faq_kat").val(id_faq_kat);
		e.preventDefault();
		//var targetUrl = $(this).attr("href");
		
		$("#dialog-ui-kat_faq_edit").dialog({
			buttons : {
				"Proses" : function() {
				  document.getElementById("form_kat_faq_edit").submit();
				},
				"Tutup" : function() {
				  $(this).dialog("close");
				}
			}
		});
		$("#dialog-ui-kat_faq_edit").dialog("open");
	});
	//end edit kategori faq
	
	//hapus kategori faq
	$("#dialog-ui-kat_faq_hapus").dialog({
		autoOpen: false,
		modal: true,
		width: 'auto'
	});
	var cek_data;
	$("#confirmLink-kat_faq_hapus").click(function(e) {
		e.preventDefault();
		id_faq_kat=$("#faq_kategori").val();
		
		<?php $url=site_url('services/faq/count');?>
		$.ajax({
			 async: false,
			 type: 'GET',
			 url: '<?=$url?>/'+id_faq_kat,
			 success: function(data) {
				cek_data=parseInt(data);
			 }
		});
		//var targetUrl = $(this).attr("href");
		//alert(cek_data);
		if(cek_data != 0){
			$("#pesan_hapus").html("Hapus gagal!, Masih ada FAQ dalam Kategori ini, silahkan hapus FAQ terlebih dahulu");	
			$("#dialog-ui-kat_faq_hapus").dialog({
				buttons : {
					"Tutup" : function() {
					  $(this).dialog("close");
					}
				}
			});
		}else{
			$("#pesan_hapus").html("Yakin ingin dihapus?");	
			$("#dialog-ui-kat_faq_hapus").dialog({
				buttons : {
					"Ya" : function() {
						<?php $url=site_url('services/faq_kategori/delete');?>
						$.ajax({
							 async: false,
							 type: 'GET',
							 url: '<?=$url?>/'+id_faq_kat,
							 success: function(data) {
								location.reload();
							 }
						});
						
					},
					"Tidak" : function() {
					  $(this).dialog("close");
					}
				}
			});
		}
		
		
		$("#dialog-ui-kat_faq_hapus").dialog("open");
	});
	//end hapus kategori faq
});

  
</script>