<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<?php include('inc/dialog-ui.php');?>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Pengaduan Online</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="example" class="product-table screen">
				<thead>
				<tr>
					
					<th class="table-header-repeat"><a >No </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Status </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Tanggal</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a>Kategori </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Kode Pengaduan </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Deskripsi</a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Sekolah</a></th>
					<th class="table-header-repeat line-left minwidth-1"><a>Lokasi</a></th>
					<th class="table-header-options line-left"><a>Action</a></th>
				</tr>
				</thead>
					<tbody>
						
						<tr>
							
							<td colspan="9" class="dataTables_empty">Loading data from server</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							
							<th>&nbsp;</th>
							<th><input id="i-1" type="text" name="search_status" value="" class="search_init" /></th>
							<th><input id="i-2" type="text" name="search_tanggal" value="" class="search_init datepicker" /></th>
							<th><input id="i-3" type="text" name="search_kategori" value="" class="search_init" /></th>
							<th><input id="i-4" type="text" name="search_kode_pengaduan" value="" class="search_init" /></th>
							<th><input id="i-5" type="text" name="search_deskripsi" value="" class="search_init" /></th>
							<th><input id="i-6" type="text" name="search_sekolah" value="" class="search_init" /></th>
							<th><input id="i-7" type="text" name="search_lokasi" value="" class="search_init" /></th>
							<th>&nbsp;</th>
						</tr>
					</tfoot>
					
				</table>
				
			<div class="clear"></div>
		 	</div>
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>
<!-- end footer -->
 
</body>
</html>