<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>user Handling</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
	
	<?php if($this->uri->segment(3)=='tambah' || $this->uri->segment(3)=='edit'):?>
 	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr valign="top">
	<td>
	
	
		<!--  start step-holder -->
		
		<!--  end step-holder -->
	
		<!-- start id-form -->
		<?php 
			if($this->uri->segment(3)=='tambah')
				$link=site_url('administrator/post_user_handling');
			elseif($this->uri->segment(3)=='edit')
				$link=site_url('administrator/post_user_handling/'.$id);
		?>
		<form action="<?=$link?>" method="post">
		<table cellspacing="0" cellpadding="0" border="0" id="id-form">
                <tbody>
                    <tr>
                        <td>Username :</td>
                        <td><input type='text' name='username' value='<?=$user->username?>'></td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>UserID :</td>
                        <td><input type='text' name='userid' value='<?=$user->userid?>'></td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>Password :</td>
                        <td><input type='text' name='password' value=''></td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>Level :</td>
                        <td>
                            <select id='level' name='level'>
                                <option value=''>-pilih-</option>
                            <?php
                                $level=$this->Select_db->level()->result();
                                foreach($level as $row):
                            ?>
                                <option value='<?=$row->id?>' <?=($user->level_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
                            <?php endforeach;?>
                            </select>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <tr id='kolom_kabkota' style='display: none;'>
                        <td>KabKota :</td>
                        <td>
                            <select id='kabkota' name='kabkota'>
                                <option value=''>-pilih-</option>
                            <?php
                                $kabkota=$this->Select_db->kabkota(array('KdProv' => 31))->result();
                                foreach($kabkota as $row):
                            ?>
                                <option value='<?=$row->KdKab?>' <?=($user->kabupaten == $row->KdKab ? 'selected' : '')?>><?=$row->NmKab?></option>
                            <?php endforeach;?>
                            </select>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <script>
                        $(document).ready(function(){
                            $('#level').change(function(){
                                if ($('#level').val()==2) {
                                    $('#kolom_kabkota').show();
                                }else{
                                    $('#kolom_kabkota').hide();
                                }
                            });
                        });
                    </script>
                    <tr>
                        <td>Role :</td>
                        <td>
                            <?php
                                $role_jenjang=array();
                                if($user->id){
                                    $query=$this->Select_db->role_jenjang(array('user_id' => $user->id))->result();
                                    
                                    foreach($query as $row){
                                        $role_jenjang[]=$row->jenjang_id;
                                    }
                                }
                                $jenjang=$this->Select_db->jenjang_sekolah()->result();
                                foreach($jenjang as $row):
                            ?>
                            <input type="checkbox" name="role[]" value="<?=$row->id?>" <?=(in_array($row->id, $role_jenjang) ? 'checked' : '')?> > <?=$row->nama?>
                            <?php endforeach;?>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>Activated :</td>
                        <td>
                            <select name='activated'>
                                <option value='1'>Ya</option>
                                <option value='0'>Tidak</option>
                            </select>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>Kirim Email :</td>
                        <td>
                            <input type='text' name='email'>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td>&nbsp</td>
                        <td>
                            <input type='submit' name='submit' value='Submit'>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                </tbody>
                </table>
	</form>
	<!-- end id-form  -->

	</td>
	
</tr>
<tr>
<td><img width="695" height="1" alt="blank" src="<?=$this->config->item('admin_img')?>/shared/blank.gif"></td>
<td></td>
</tr>
</tbody></table>
	<?php elseif($this->uri->segment(3)=='view'):?>
	<?=$berita->judul?>
	<?=$berita->tanggal?>
	<?=$berita->isi?>
	<?php endif;?>
<div class="clear"></div>
 

</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>
<!-- end footer -->
 
</body>
</html>