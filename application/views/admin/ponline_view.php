<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->

<!-- End: page-top-outer -->
	

 
<!--  start nav-outer-repeat................................................................................................. START -->

<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Pengaduan Online </h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			<?php if($this->uri->segment(3)=="view"):?>
			<table cellspacing="0" cellpadding="0" border="0" id="id-form">
			  <tr>
                <th valign="top">Status</th>
			    <td><?=$row->status?></td>
			    </tr>
              <tr>
                <th valign="top">Tanggal</th>
                <td><?=mysqldatetime_to_date($row->tanggal, $format = "d/m/Y, H:i:s")?></td>
                </tr>
              <tbody>
                <tr>
                  <th valign="top">Lokasi:</th>
                  <td><?=$row->lokasi;?></td>
                  </tr>
                <tr>
                  <th valign="top">Nama Sekolah </th>
                  <td><?=$row->sekolah?></td>
                  </tr>
                <tr>
                  <th valign="top">Kategori:</th>
                  <td><?=$row->kategori?></td>
                  </tr>
                <tr>
                  <th valign="top">Profesi:</th>
                  <td><?=$row->profesi?></td>
                  </tr>
                <tr>
                  <th valign="top">Nama :</th>
                  <td><?=$row->nama;?></td>
                  </tr>
                <tr>
                  <th valign="top">Email:</th>
                  <td><?=$row->email;?></td>
                  </tr>
                <tr>
                  <th valign="top">Telp:</th>
                  <td><?=$row->telp;?></td>
                  </tr>
                <tr>
                  <th valign="top">Alamat:</th>
                  <td><?=$row->alamat;?></td>
                  </tr>
                <tr>
                  <th valign="top">Deskripsi:</th>
                  <td><?=$row->deskripsi;?></td>
                  </tr>
              </tbody>
			  </table>
				<?php endif;?>
			</div>
			<!--  end table-content  -->
	
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->

</div>
<!--  end content-outer........................................................END -->


    
<!-- start footer -->         

<!-- end footer -->
 
</body>
</html>