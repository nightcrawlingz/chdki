<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap 101 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=$this->config->item('handling_plugin')?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
    
    .navbar {
    background: url("http://www.bos.site/pengaduan/media/handling/image/header-bg.png") repeat-x;
    min-height: 90px;
    }
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=$this->config->item('handling_js')?>/html5shiv.js"></script>
      <script src="<?=$this->config->item('handling_js')?>/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <!-- Static navbar -->
    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="page-header" style="margin: 0; border-bottom: none">
              <a href="#" style="float:left"><img style="height:70px;" src="http://bos.kemdikbud.go.id/pengaduan/media/home/images/logo_bos.png"></a>
              <h2 style="margin-top: 8px">Pelayanan & Penanganan Pengaduan Masyarakat</h2></div>
        </div>
          <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><p class="navbar-text" style="color: #000000">Selamat Datang,</p></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-weight: bold;
text-transform: capitalize;
font-size: 126%; color: #333">Pusat SD <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-book"></span> Manual</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span> bantuan</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Keluar</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  
  
    <ul class="nav nav-tabs">
    <li class="active"><a href="#">User Management</a></li>
    <li><a href="#"><span class="badge">14</span> Data Pengaduan</a></li>
    <li><a href="#">Laporan</a></li>
    <li><a href="#">User Info</a></li>
    <li><a href="#">Pemberian Kode Registrasi</a></li>
  </ul>
  <h1>Hello, world!</h1>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="//code.jquery.com/jquery.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?=$this->config->item('handling_plugin')?>/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>