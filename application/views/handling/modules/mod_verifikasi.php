<?php $filter=$this->session->userdata('filter');?>

<div id="wrap">
<div class="container">
  <?php
  $notif=$this->session->flashdata('success');
  if($notif):
  ?>
    <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?=$notif['msg']?></div>
  <?php endif;?>
  <div>&nbsp</div>
  <div class="table-responsive">
  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="verifikasi">
      <thead>
	  <tr>              
	      <th>&nbsp;</th>
	      <th><input id="i-1" type="text" name="search_1" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-2" type="text" name="search_2" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-3" type="text" name="search_3" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-4" type="text" name="search_4" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-5" type="text" name="search_5" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-6" type="text" name="search_6" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-7" type="text" name="search_7" value="" class="form-control input-sm search_init" /></th>
	      <th>&nbsp;</th>
	  </tr>
          <tr>
              <th>#</th>
              <th>Program</th>
              <th>Kategori</th>
              <th>Tanggal</th>
              <th>Daerah</th>
              <th>Deskripsi</th>
              <th>Publikasi</th>
              <th>Verifikasi</th>
              <th>Action</th>
          </tr>
      </thead>
      <tbody>					
        <tr>                    
          <td colspan="9" class="dataTables_empty">Loading data from server</td>
        </tr>
      </tbody>
      <tfoot>
        
      </tfoot>
  </table>
  </div>
</div>
</div>