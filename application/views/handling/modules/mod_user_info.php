<div id="wrap">
<div class="container">
  <?php
  $notif=$this->session->flashdata('success');
  if($notif):
  ?>
    <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?=$notif['msg']?></div>
  <?php endif;?>
  
  <div>&nbsp</div>
  <form class="form-horizontal" role="form" action='<?=site_url('handling/user_info/post_update')?>' method='post'>
  <div class="form-group">
    <label for="nama" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name='nama' id="nama" placeholder="Nama" value='<?=$user_handling->nama?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="nip" class="col-sm-2 control-label">NIP</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name='nip' id="nip" placeholder="NIP" value='<?=$user_handling->nip?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="jabatan_dinas" class="col-sm-2 control-label">Jabatan di Dinas</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="jabatan_dinas" name='jabatan_dinas' placeholder="Jabatan di Dinas" value='<?=$user_handling->jabatan_dinas?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="jabatan_bos" class="col-sm-2 control-label">Jabatan di Tim BOP KJP</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="jabatan_bos" name='jabatan_kjp_bop' placeholder="Jabatan di Tim BOS" value='<?=$user_handling->jabatan_kjp_bop?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-5">
      <input type="email" class="form-control" id="email" name='email' placeholder="Email" value='<?=$user_handling->email?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="alamat_kantor" class="col-sm-2 control-label">Alamat Kantor</label>
    <div class="col-sm-5">
      <textarea class="form-control" name='alamat_kantor' id='alamat_kantor' rows="3"><?=$user_handling->alamat_kantor?></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="alamat_rumah" class="col-sm-2 control-label">Alamat Rumah</label>
    <div class="col-sm-5">
      <textarea class="form-control" name='alamat_rumah' id='alamat_rumah' rows="3"><?=$user_handling->alamat_rumah?></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="telp_kantor" class="col-sm-2 control-label">Telp Kantor</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="telp_kantor" name='telp_kantor' placeholder="Telp Kantor" value='<?=$user_handling->telp_kantor?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="fax_kantor" class="col-sm-2 control-label">Fax Kantor</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="fax_kantor" placeholder="Fax Kantor" name='fax_kantor' value='<?=$user_handling->fax_kantor?>'>
    </div>
  </div>
  <div class="form-group">
    <label for="handphone" class="col-sm-2 control-label">No Handphone</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="handphone" name='handphone' placeholder="No Handphone" value='<?=$user_handling->handphone?>'>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-5">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
  </form>
</div>
</div>