<?php
    $role_jenjang = $this->role_jenjang;
    $role_program = $this->role_program;
    $role_level = $this->role_level;
    
    $param['status'] = 1;
    $param['program'] = $role_program;
    $param['jenjang'] = $role_jenjang;
    $param['level'] = $role_level;
    $query = $this->pengaduan_db->get_all($param);
    $num_pending = $query->num_rows();
?>
  <ul class="nav nav-tabs">
    <li <?=($this->uri->segment(2)=='verifikasi' ? 'class="active"' : '')?>><a href="<?=site_url('handling/verifikasi')?>">Verifikasi</a></li>
    <li <?=($this->uri->segment(2)=='pengaduan' ? 'class="active"' : '')?>><a href="<?=site_url('handling/pengaduan')?>"><?=$num_pending ? '<span class="badge">'.$num_pending.'</span>' : ''?> Data Pengaduan</a></li>
    <li <?=($this->uri->segment(2)=='laporan' ? 'class="active"' : '')?>><a href="<?=site_url('handling/laporan')?>">Laporan</a></li>
    <?php if($this->role_level->level_id == 1 || $this->role_level->level_id == 2):?>
      <li <?=($this->uri->segment(2)=='akses_log' ? 'class="active"' : '')?>><a href="<?=site_url('handling/akses_log')?>">Akses Log</a></li>
    <?php endif;?>
    <li <?=($this->uri->segment(2)=='user_info' ? 'class="active"' : '')?>><a href="<?=site_url('handling/user_info')?>">User Info</a></li>
    <li <?=($this->uri->segment(2)=='manual' ? 'class="active"' : '')?>><a href="<?=site_url('handling/manual')?>">Manual Aplikasi</a></li>
  </ul>