<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1" style='float:left;'>
			
			<div class="judul_page" >
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/graph/r1/all/all/all')?>" class="current">Statistik Pengaduan</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" />Statistik Pengaduan
			</div>
	
		<?php
		$filter = array(
		    'tahun' => $this->uri->segment(4),
		    'provinsi' => $this->provinsi,
		    'kabkota' => $this->uri->segment(5)
		);
		$page_report = $this->uri->segment(3);
		if($page_report == 'report_kategori')
		    $report = $this->report_db->kategori($filter)->result();
		elseif($page_report == 'report_sumber_info')
		    $report = $this->report_db->sumber_info($filter)->result();
		elseif($page_report == 'report_status')
		    $report = $this->report_db->status($filter)->result();
		elseif($page_report == 'report_program')
		    $report = $this->report_db->program($filter)->result();
		elseif($page_report == 'report_kategori_status')
		    $report = $this->report_db->kategori_status($filter)->result();
		elseif($page_report == 'report_media')
		    $report = $this->report_db->media($filter)->result();
		elseif($page_report == 'report_program_status')
		    $report = $this->report_db->program_status($filter)->result();
		elseif($page_report == 'report_pelaku_status')
		    $report = $this->report_db->pelaku_status($filter)->result();
		elseif($page_report == 'report_wilayah_status'){
		    $report = $this->report_db->wilayah_status($filter)->result();
		}
		//echo $this->db->last_query();
		$chart = $page_report;
		
	    ?>
	    <style>
		.table-responsive .table{
		    float:left;
		}
	    </style>
	    <div id="show_chart" align="center" style="height:450px; float:left"><!--Chart Di Load disini--></div>
	    <?=$this->load->view('handling/modules/chart/'.$chart,array('report' =>$report));?>
		<div style="clear: both;"></div><br>
		<?php include('inc/share_button.php');?>
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
     <?php include('inc/menu_kanan.php');?>
	  <!-- sidebar end -->
      <div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
	
</script>
      <!--page laporan-->
      <?php $page_report = $this->uri->segment(3)?>
      <?php if($this->uri->segment(2)=='graph'):?>
  
	      <!--report kategori-->
	      <?php if($page_report=='report_kategori'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->kategori."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->kategori."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Kategori Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report kategori-->
	      
	      <!--report sumber informasi-->
	      <?php if($page_report=='report_sumber_info'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->sumber."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->sumber."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN SUMBER INFORMASI<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Sumber Informasi',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report sumber informasi-->
	      
	      <!--report status-->
	      <?php if($page_report=='report_status'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->status."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->status."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Status Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report status-->
	      
	      <!--report program-->
	      <?php if($page_report=='report_program'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->program."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->program."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Program Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report program-->
	      
	      <!--report kategori status-->
	      <?php if($page_report=='report_kategori_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->kategori."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Kategori dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report kategori status-->
	      
	      <!--report media-->
	      <?php if($page_report=='report_media'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->media."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->media."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN MEDIA PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Media Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report media-->
	      
	      <!--report program status-->
	      <?php if($page_report=='report_program_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->program."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Program dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report program status-->
	      
	      <!--report pelaku status-->
	      <?php if($page_report=='report_pelaku_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->pelaku."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Pelaku dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report pelaku status-->
	      
	      <!--report wilayah-->
	      <?php if($page_report=='report_wilayah_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->wilayah."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  <?php /*
		  plot2 = $.jqplot('show_chart', [pending, proses, selesai], {
		      animate: !$.jqplot.use_excanvas,
		      seriesDefaults: {
			      renderer:$.jqplot.BarRenderer,
		      pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
		      shadowAngle: 135,
			      rendererOptions: {
			  barDirection: 'horizontal'
		      }
		      },
		      axes: {
			      
			      yaxis: {
				      renderer: $.jqplot.CategoryAxisRenderer,
				      ticks: ticks,
				      
			      },
			      xaxis: {
				      min:0,
				      tickInterval: 10,
				      tickOptions: { formatString:'%d' }
			      },
		      },
		      legend: {
			      show: true,
			      location: 'ne',
			      placement: 'inside'
		      },
		      series:[
			  {label:'Pending'},
			  {label:'Proses'},
			  {label:'Selesai'}
		      ],
		      title:{
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		    }
		  });
		  */
		  ?>
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'bar'
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks,
			  title: {
			      text: null
			  }
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Pengaduan per Wilayah',
			      align: 'high'
			  },
			  labels: {
			      overflow: 'justify'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  bar: {
			      dataLabels: {
				  enabled: true
			      }
			  }
		      },
		      
		      credits: {
			  enabled: false
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
		      }, {
			  name: 'Proses',
			  data: proses
		      }, {
			  name: 'Selesai',
			  data: selesai
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report wilayah-->
	      <!--end report-->
  <?php endif;?>
  <!--end page laporan-->
</body>
</html>