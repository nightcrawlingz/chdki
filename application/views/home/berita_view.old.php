<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php include("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
      <?php require_once("inc/menu.php"); ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="mainbar" style="width:inherit">
	  <div class="article">
	  	<h2><img src="<?=$this->config->item('home_img')?>/berita.png" width="48" style="border:0; padding:0;"/><a href="<?=site_url('home/berita')?>"> Berita</a></h2>
	  </div>
	  <?php if($this->uri->segment(3)!='view'):?>
	  <?php foreach($berita->result() as $row):?>
        <div class="article">
          <h2><a href="<?=site_url('home/berita/view/'.$row->id)?>" ><?=$row->judul?></a></h2>
          <div class="clr"></div>
          <p class="infopost">Di Posting Tanggal <?=mysqldatetime_to_date($row->tanggal, $format = "d/m/Y, H:i:s")?> oleh <?=$row->nama?></p>
		  <?=$row->ringkasan?>
          <p><a href="<?=site_url('home/berita/view/'.$row->id)?>" class="MBT-readmore10">Selanjutnya..</a></p>
		  
        </div>
        
				
			<?php endforeach;?>
		<?php elseif($this->uri->segment(3)=='view'):?>
		 <div class="article">
          <h2><?=$berita->judul?></h2>
          <div class="clr"></div>
          <p class="infopost">Di Posting Tanggal <?=mysqldatetime_to_date($berita->tanggal, $format = "d/m/Y, H:i:s")?> oleh <?=$berita->nama?></p>
		  <?=$berita->isi;?>
		  	<style>
				#share #fb, #share #twitter, #share #gplus {
					width:70px;
					height:50px;
					float:left;
				}
				
			</style>
		  	<div id="share">
				<div id="fb">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=279477298749825";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<fb:share-button type="box_count"></fb:share-button>
				</div>
				<div id="twitter">
					<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
					if(!d.getElementById(id)){js=d.createElement(s);
					js.id=id;js.src="//platform.twitter.com/widgets.js";
					fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<div id="gplus">
					<!-- Place this tag in your head or just before your close body tag. -->
					<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
					
					<!-- Place this tag where you want the share button to render. -->
					<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60"></div>
				</div>
			</div>
        
		</div>
		
		<?php endif;?>
		<?php if($this->uri->segment(3)=='list' || $this->uri->segment(3)==''):?>
		<?=$this->pagination->create_links();?>
		<?php endif;?>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
  </div>
</div>
</body>
</html>
