<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<?php include('inc/slider.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
	
	<div class="sidebarkiri">
		
		
	<?php include('inc/menu_kanan_home.php');?>
	</div>
	<!-- sidebarkiri end -->
	
	<div class="maincontent">
	
		<!--------kiri----------------->
	<div id="content_tengah">
	
	 
      	<div class="page1-col-tengah">
            <h2 class="h2"></h2>
            <div id="show_chart" align="center" style="height:450px"><!--Chart Di Load disini--></div>
            <?php $report = $data['report'] = $this->report_db->kategori_status()->result();?>
            <?=$this->load->view('handling/modules/chart/report_kategori_status', $data);?>
            
            <!--report kategori status-->
	      <?php
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->kategori."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI DAN STATUS PENGADUAN<br>TAHUN <?=(date('Y')==2014 ? '2014' : '2011 s/d '.date('Y'))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Kategori dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <!--./report kategori status-->
      </div>
	</div>
	<!--------endkiri----------------->
	
	</div>
	<!-- maincontent end -->
	
	<div class="sidebarkanan">
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/berita')?>">
						<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Berita
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($berita->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/berita/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/berita')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
		<!-- Pengumuman -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/pengumuman')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263507_agt_announcements.png" /> Pengumuman
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				 <ul>
					  <?php foreach($pengumuman->result() as $row):?>
					    <?php if($row->published):?>
						<li><a href="<?=site_url('home/pengumuman/view/'.$row->id)?>"><?=$row->title?></a><br />
						  <?php //$row->ringkasan?>
						</li>
					    <?php endif;?>
					  <?php endforeach;?>
			  	
				</ul>
	              <a href="<?=site_url('home/pengumuman')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- Pengumuman END -->
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/peraturan')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263626_administrative-docs.png" /> Peraturan Perundangan
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($peraturan->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/peraturan/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/peraturan')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/publikasi')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263789_human-folder-public.png" /> Publikasi
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($publikasi->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/publikasi/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/berita')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
	
        
	
	</div>
	<!-- sidebarkanan end -->
      
      
      
      
      
      <div class="clear"></div>
    </div>
</section>
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>	
<script>
	Cufon.now();
</script>
</body>
</html>