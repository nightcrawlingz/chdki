<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/berita')?>" class="current">Berita</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Berita
			</div>
	<?php if($this->uri->segment(3)!='view'):?>
		<?php foreach($berita->result() as $row):?>
			<h1><a href="<?=site_url('home/berita/view/'.$row->id)?>" ><?=$row->title?></a></h1>
			<h4>Di Posting Tanggal <?=mysqldatetime_to_date($row->last_update, $format = "d/m/Y, H:i:s")?> oleh <?=$row->nama?></h4>
			<?php
			  //get image klo ada
			  preg_match_all('/<img[^>]+>/i',$row->content, $result);
			  if($result[0])
			    echo '<p>'.$result[0][0].'</p><br>';
			?>
			<?=$this->initlib->character_limiter(strip_tags($row->content), 500,'<a class="button" href="'.site_url('home/berita/view/'.$row->id).'">Baca Selanjutnya</a>');?>
		<?php endforeach;?>
	<?php elseif($this->uri->segment(3)=='view'):?>
		<h1><?=$berita->title?></h1>
		<h4>Di Posting Tanggal <?=mysqldatetime_to_date($berita->last_update, $format = "d/m/Y, H:i:s")?> oleh <?=$berita->nama?></h4>
		<?=$berita->content;?>
		<?php include('inc/share_button.php');?>
	<?php endif;?>
	<?php if($this->uri->segment(3)=='list' || $this->uri->segment(3)==''):?>
		<?=$this->pagination->create_links();?>
	<?php endif;?>
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
     <?php include('inc/menu_kanan.php');?>
	  <!-- sidebar end -->
      <div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
</script>
</body>
</html>