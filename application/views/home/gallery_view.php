<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<style>

.page1-col1, .judul_page, .container_12 .grid_8  {
    width: 1000px;
}
</style>
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/gallery')?>" class="current">Gallery</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Gallery
			</div>
	
		
		<!-- Start Advanced Gallery Html Containers -->
				<div id="gallery" class="content">
					<div id="controls" class="controls"></div>
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="slideshow" class="slideshow"></div>
					</div>
					<div id="caption" class="caption-container"></div>
				</div>
				<div id="thumbs" class="navigation">
					<ul class="thumbs noscript">
						<?php foreach($gallery->result() as $row):?>
                        <?php
                        	$image_small=site_url('media/upload/images/gallery/.thumbs/'.$row->nama."_s.".$row->ext);
							$image_medium=site_url('media/upload/images/gallery/.thumbs/'.$row->nama."_m.".$row->ext);
							$image_large=site_url('media/upload/images/gallery/'.$row->nama_file);
						?>
						<li>
							<a class="thumb" href="<?=$image_medium?>" title="<?=html_entity_decode($row->judul)?>">
								<img src="<?=$image_small?>" alt="<?=html_entity_decode($row->judul)?>" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="<?=$image_large?>">Download Original</a>
								</div>
								<div class="image-title"><?=html_entity_decode($row->judul)?></div>
								<div class="image-desc"><?=html_entity_decode($row->keterangan)?></div>
							</div>
						</li>
                        <?php endforeach;?>
					</ul>
				</div>
				
				<!-- End Advanced Gallery Html Containers -->
		<div style="clear: both;"></div>
		<?php include('inc/share_button.php');?>
	
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
	  <!-- sidebar end -->
<div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
</script>
</body>
</html>