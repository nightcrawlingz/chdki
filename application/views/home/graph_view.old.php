<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
	  <?php require_once("inc/menu.php") ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="sidebar1">
        <?php require_once('inc/form_chart.php') ?>
      </div>
	  <div class="mainbar1">
        <div class="article">
          
          <div class="clr"></div>
          <p class="infopost"><!--Posted <span class="date">on 11 sep 2018</span> by <a href="#">Owner</a> &nbsp;|&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> with <a href="#" class="com">Comments <span>11</span></a>--></p>
		  <div align="center">
		  
	</div>
	<?php
	$kategori=array(
				'r1' => 'Kategori',
				'r2' => 'Sumber Informasi',
				'r3' => 'Jumlah & Status',
				'r4' => 'Kategori & Status',
				'r5' => 'Pelaku & Status',
				'r6' => 'Status Provinsi',
				'r7' => 'Media'
			);
	?>
	<h2><span>Pengaduan Berdasarkan <?=$kategori[$this->uri->segment(3)]?></span></h2>
			<div id="chart" class="FusionCharts-container" align="center"><!--Chart Di Load disini--></div>
			<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>
			<style>
			div.chart_table table{
				border-collapse:collapse;
			}
			div.chart_table table, td, th{
				border:1px solid black;
				color:#000000;
			}
			
			</style>
			<div align="center" class="chart_table"><?=$graph_table?></div>
            <p>&nbsp;</p>
        </div>
        <p class="pages"><!--<small>Page 1 of 2 &nbsp;&nbsp;&nbsp;</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a>--></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
  </div>
</div>
</body>
</html>
