<?php 
// kategori & status
if($report->num_rows() != 0):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_pelaku" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
<?php
	$row=$report->row();
	$proses[]=$row->proses_komite;
	$proses[]=$row->proses_guru;
	$proses[]=$row->proses_bensek;
	$proses[]=$row->proses_lsm;
	$proses[]=$row->proses_dinas;
	$proses[]=$row->proses_uptd;
	$proses[]=$row->proses_lain;
	
	$selesai[]=$row->selesai_komite;
	$selesai[]=$row->selesai_guru;
	$selesai[]=$row->selesai_bensek;
	$selesai[]=$row->selesai_lsm;
	$selesai[]=$row->selesai_dinas;
	$selesai[]=$row->selesai_uptd;
	$selesai[]=$row->selesai_lain;
	
	$kat_pelaku=array("'Komite Sekolah'","'Guru/Pegawai Sekolah'","'Kepala/Bendahara Sekolah'", "'LSM'","'Dinas Pendidikan'","'UPTD'","'Lain-Lain'");
?>
$(document).ready(function(){
	var proses = [<?=implode(",",$proses)?>];
	var selesai = [<?=implode(",",$selesai)?>];
	var ticks = [<?=implode(",",$kat_pelaku)?>];
	 
	plot2 = $.jqplot('chart_pelaku', [proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
                shadowAngle: 135,
			rendererOptions: {
                    barDirection: 'horizontal'
                }
		},
		axes: {
			
			yaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks,
				
			},
			xaxis: {
				min:0,
				tickInterval: 20,
				tickOptions: { formatString:'%d' }
			},
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENGADUAN BERDASARKAN PELAKU <br>& STATUS PENANGANAN TAHUN <?=($this->uri->segment(6)=='all' ? '2011 s/d '.date('Y') : $this->uri->segment(6))?>'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>

<div align="center" class="chart_table">
<table>
<tr>
	<td><strong>Pelaku</strong></td>
	<td><strong>Proses</strong></td>
	<td><strong>Selesai</strong></td>
	<td><strong>Total</strong></td>
</tr>
<?php $row=$report->row();?>
<tr>
	<td>Komite Sekolah</td>
	<td><?=$row->proses_komite?></td>
	<td><?=$row->selesai_komite?></td>
	<td><?=$row->proses_komite+$row->selesai_komite?></td>
</tr>
<tr>
	<td>Guru/Pegawai Sekolah</td>
	<td><?=$row->proses_guru?></td>
	<td><?=$row->selesai_guru?></td>
	<td><?=$row->proses_guru+$row->selesai_guru?></td>
</tr>
<tr>
	<td>Kepala/Bendahara Sekolah</td>
	<td><?=$row->proses_bensek?></td>
	<td><?=$row->selesai_bensek?></td>
	<td><?=$row->proses_bensek+$row->selesai_bensek?></td>
</tr>
<tr>
	<td>LSM</td>
	<td><?=$row->proses_lsm?></td>
	<td><?=$row->selesai_lsm?></td>
	<td><?=$row->proses_lsm+$row->selesai_lsm?></td>
</tr>
<tr>
	<td>Dinas Pendidikan</td>
	<td><?=$row->proses_dinas?></td>
	<td><?=$row->selesai_dinas?></td>
	<td><?=$row->proses_dinas+$row->selesai_dinas?></td>
</tr>

<tr>
	<td>UPTD</td>
	<td><?=$row->proses_uptd?></td>
	<td><?=$row->selesai_uptd?></td>
	<td><?=$row->proses_uptd+$row->selesai_uptd?></td>
</tr>
<tr>
	<td>Lain-Lain</td>
	<td><?=$row->proses_lain?></td>
	<td><?=$row->selesai_lain?></td>
	<td><?=$row->proses_lain+$row->selesai_lain?></td>
</tr>
</table>
</div>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>