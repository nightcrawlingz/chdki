<?php 
// sumber informasi
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_sumber_info" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var data = [['Orang Tua Siswa',820],['Komite Sekolah',93],['Guru-Pegawai Sekolah',740],['Kepala Sekolah',177],['Tim Manajemen BOS',260],['Media Massa',5],['Hasil Audit',1],['Lain-lain',419]];
	 
	plot2 = $.jqplot('chart_sumber_info', [data], {
		seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            trendline:{ show:false },
            rendererOptions: { padding: 8, sliceMargin: 4, showDataLabels: true }
        },
        legend:{
            show:true,
            placement: 'inside',
            location:'ne'
        },
		
	   title:{
			text:'PENGADUAN BERDASARKAN SUMBER INFORMASI<br>TAHUN 2011 s/d 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>


<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<th>Kategori</th>
	<th>Total</th>
</tr>
<tr>
	<td>Orang Tua Siswa</td>
	<td>820</td>
	</tr>
<tr>
	<td>Komite Sekolah</td>
	<td>93</td>
	</tr>
<tr>
	<td>Guru-Pegawai Sekolah</td>
	<td>740</td>
	</tr>
<tr>
	<td>Kepala Sekolah</td>
	<td>177</td>
	</tr>
<tr>
	<td>Tim Manajemen BOS</td>
	<td>260</td>
	</tr>
<tr>
	<td>Media Massa</td>
	<td>5</td>
	</tr>
<tr>
	<td>Hasil Audit</td>
	<td>1</td>
	</tr>
<tr>
	<td>Lain-lain</td>
	<td>419</td>
	</tr>
<tr>
	<td><strong>Total</strong></td>
	<td><strong>2515</strong></td>
</tr>
</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>