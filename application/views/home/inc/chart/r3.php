<?php 
// jumlah & status
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_jumlah_status" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var data = [['Proses 0-180 Hari',223],['Proses 181-365 Hari',49],['Proses lebih 365 Hari',156],['Selesai',2368]];
	 
	plot2 = $.jqplot('chart_jumlah_status', [data], {
		seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            trendline:{ show:false },
            rendererOptions: { padding: 8, sliceMargin: 4, showDataLabels: true }
        },
        legend:{
            show:true,
            placement: 'inside',
            location:'ne'
        },
		
	   title:{
			text:'JUMLAH & PENGADUAN BERDASARKAN STATUS<br>TAHUN 2011 s/d 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>

<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<th>Pengaduan</th>
	<th>Total</th>
</tr>
<tr>
	<td>Proses 0-180 Hari</td>
	<td>223</td>
</tr>
<tr>
	<td>Proses 181-365 Hari</td>
	<td>49</td>
</tr>
<tr>
	<td>Proses Lebih 365 Hari</td>
	<td>156</td>
</tr>
<tr>
	<td>Selesai</td>
	<td>2368</td>
</tr>
<tr>
	<td><strong>Total</strong></td>
	<td>2932</td>
</tr>
</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>