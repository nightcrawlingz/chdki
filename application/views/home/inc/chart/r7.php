<?php 
// sumber informasi
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_media" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var data = [['Pengaduan Online',127],['SMS',11],['Lainnya',9]];
	 
	plot2 = $.jqplot('chart_media', [data], {
		seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            trendline:{ show:false },
            rendererOptions: { padding: 8, sliceMargin: 4, showDataLabels: true }
        },
        legend:{
            show:true,
            placement: 'inside',
            location:'ne'
        },
		
	   title:{
			text:'JUMLAH PENGADUAN BERDASARKAN MEDIA<br>TAHUN 2011 s/d 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>

<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td><strong>Media</strong></td>
	<td><strong>Total</strong></td>
</tr>
<tr>
	<td>Pengaduan Online</td>
	<td>127</td>
	</tr>
<tr>
	<td>SMS</td>
	<td>11</td>
	</tr>
<tr>
	<td>Lainnya</td>
	<td>9</td>
	</tr>
<tr>
	<td><strong>Total</strong></td>
	<td><strong>147</strong></td>
</tr>
</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>