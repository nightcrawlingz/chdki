<div class="fbg">
    <div class="fbg_resize" style="border:none; background:none;">
      <div class="col c1">
        <h2>&nbsp;</h2>
        <a href="#"></a> <a href="#"></a> <a href="#"></a> <a href="#"></a> <a href="http://kemdikbud.go.id"><img src="<?=$this->config->item("home_img")?>/logo_kemdiknas.png" alt="" style="border:none; background:none"/></a> <a href="#"></a> </div>
      <div class="col c2">
	  <style>
	  .ul_solid {
    border-bottom: 1px solid #E6E6E6;
	background:none;
}
	  </style>
        <h2>Resource links </h2>
		<ul class="ex_menu ul_solid">
			<li><a title="http://www.kemdikbud.go.id/" href="http://www.kemdikbud.go.id/"><strong>www.kemdikbud.go.id</strong><br /><span style="color:#000000;">Kementerian Pendidikan dan Kebudayaan Republik Indonesia</span></a></li>
			<li><a title="http://bos.kemdikbud.go.id/" href="http://bos.kemdikbud.go.id/"><strong>bos.kemdikbud.go.id</strong><br /><span style="color:#000000;">Bantuan Operasional Sekolah</span></a></li>
		</ul>
		
		    <h2>Social Network</h2>
            <ul class="ex_menu ul_solid">
              <a href="http://twitter.com/#!/programbos" target="_blank"><img src="http://bos.kemdikbud.go.id/media/home/images/twitter.png" title="Twitter" alt="Twitter" border="0"></a> <a href="http://www.facebook.com/pages/Program-BOS/261858107227613" target="_blank"><img src="http://bos.kemdikbud.go.id/media/home/images/facebook.png" title="Facebook" alt="Facebook" border="0"></a>
            </ul>
	      
		
      </div>
      <div class="col c3">
        <h2>Hubungi Kami </h2>
		<div>
                  <p>
	<span>Kompleks Kementerian Pendidikan Nasional<br>
	Jl. Jendral Sudirman Gedung E lt 15-17, Senayan Jakarta- Indonesia</span></p>
<ul>
	<li>
		<span><strong>Alamat web </strong>:&nbsp; <span><a target="_blank" href="http://bsm.kemdikbud.go.id">http://bsm.kemdikbud.go.id</a></span></span></li>
	<li>
		<span><strong>Nomor telepon</strong> : 0-800-140-1299 dan 0-800-140-1276 (bebas pulsa) 021-5725980 dan 021-5725632</span></li>
	<li>
		<span><strong>Faksimil </strong>: 021-5731070, 021-5725645, 021-5725635</span></li>
	<li>
		<span><strong>Email </strong>: <a href="mailto:bsm@kemdikbud.go.id">bsm@kemdikbud.go.id</a></span></li>
</ul>
			  </div><br />
        
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf">&copy; 2011 - <?=date('Y')?> <span id="spanYear"></span> <a href="http://bos.kemdikbud.go.id">bos.kemdikbud.go.id</a>. All rights reserved</p>
      <p class="rf"><!--Layout by Rocket <a href="http://www.rocketwebsitetemplates.com/">Website Templates</a>--></p>
      <div class="clr"></div>
    </div>
	
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://bos.kemdiknas.go.id/analytics/" : "http://bos.kemdiknas.go.id/analytics/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://bos.kemdiknas.go.id/analytics/piwik.php?idsite=2" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
<!--end fancybox-->
