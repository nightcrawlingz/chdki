
			
        
        <div class="tabs_home">
        
            <ul class="nav">
               <li class="selected"><a href="#tab-1">Pengaduan</a></li>
               <li><a href="#tab-2">Statistik</a></li>
            </ul>
            
            <div id="tab-1" class="tab-content-home">
               <div class="inner">
                <p class="color-1">Pengaduan Berdasarkan</p>
				
				<form method="post" action="<?=site_url('home/post_graph')?>">
				<?php
					$kategori=array(
						'r1' => 'Kategori',
						'r2' => 'Sumber Informasi',
						'r3' => 'Jumlah & Status',
						'r4' => 'Kategori & Status',
						'r5' => 'Pelaku & Status',
						'r6' => 'Status Daerah',
						'r7' => 'Media'
					);
				  ?>
				<table id="t_form_home" border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td> 
							<select name="chart">
							  <?php foreach($kategori as $key => $value):?>
							  <?php if($this->uri->segment(3)==$key):?>
							  <option value="<?=$key?>" selected="selected">
							  <?=$value?>
							  </option>
							  <?php else:?>
							  <option value="<?=$key?>" >
							  <?=$value?>
							  </option>
							  <?php endif;?>
							  <?php endforeach;?>	
							</select>
						</td>
					</tr>
					
					<tr>
						<td>Kab / Kota</td>
					</tr>
					<tr>
						<td> 
							<select id="kabkota" name="kabkota" >
								<option value="all">-Semua-</option>
								<?php foreach($kabkota->result() as $row):?>
								<option value="<?=$row->id?>">
								  <?=$row->name?>
								</option>
								<?php endforeach;?>
								
							  </select>
						</td>
					</tr>
					<tr>
						<td>Tahun</td>
					</tr>
					<tr>
						<td> 
							<select id="tahun" name="tahun" >
							<option value="all">-Semua-</option>
							<?php for($i=2014;$i<=date('Y');$i++):?>
							<?php if($this->uri->segment(2)=='graph' && $this->uri->segment(6)==$i):?>
								<option value="<?=$i?>" selected="selected"><?=$i?></option>
							<?php else:?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif;?>
							<?php endfor;?>
							</select>
						</td>
					</tr>
					<tr>
                	<td>
                    <input type="submit"  class="button_proses" value="Proses" /></td>
                </tr>
				</table>
                            
                </form>
                    
                    
               </div>  
            </div>
            <div id="tab-2" class="tab-content-home">
               <div class="inner">
               		
                        	<p class="color-1">Statistik Pengunjung.</p>
                          
               </div>  
            </div>
            
        </div>