<div class='sidebarkanan' style='width:350px'>
<div class="grid_4">
          
              
              <div class="menu_box">
				<ul>
					<li><a href="<?=site_url('home/berita')?>"><img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Berita</a></li>
					<li><a href="<?=site_url('home/pengumuman')?>"><img width="24" src="<?=$this->config->item('home_img')?>/1401263507_agt_announcements.png" /> Pengumuman</a></li>
					<li><a href="<?=site_url('home/peraturan')?>"><img width="24" src="<?=$this->config->item('home_img')?>/1401263626_administrative-docs.png" /> Peraturan Perundangan</a></li>
					<li><a href="<?=site_url('home/publikasi')?>">
					<img width="24" src="<?=$this->config->item('home_img')?>/1401263789_human-folder-public.png" /> 
					Publikasi</a></li>
					<!--<li><a href="#"><img src="<?=$this->config->item('home_img')?>/icon_document.png" /> Dokumen</a></li>
					<li><a href="<?=site_url('home/gallery')?>"><img src="<?=$this->config->item('home_img')?>/icon_gallery.png" /> Gallery</a></li>-->
					
				</ul>
			  </div>
			  
            
			  
          </div>
      
	  <div class="grid_4">
        
        <div class="tabs">
            <ul class="nav">
               <li class="selected"><a href="#tab-1">Pengaduan</a></li>
               <li><a href="#tab-2">Statistik</a></li>
            </ul>
            <div id="tab-1" class="tab-content">
               <div class="inner">
                <p class="color-1">Pengaduan Berdasarkan</p>
				
				<form method="post" action="<?=site_url('home/post_graph')?>">
				<?php
					$kategori=array(
						'r1' => 'Kategori',
						'r2' => 'Sumber Informasi',
						'r3' => 'Jumlah & Status',
						'r4' => 'Kategori & Status',
						'r5' => 'Pelaku & Status',
						'r6' => 'Status Daerah',
						'r7' => 'Media'
					);
				  ?>
				<table id="t_form" border="0" width="256" cellpadding="0" cellspacing="0">
					<tr>
						<td> 
							<select name="chart">
							  <?php foreach($this->Select_db->report_kategori()->result() as $row):?>
							  <?php if($this->uri->segment(3)==$row->id):?>
							  <option value="<?=$row->id?>" selected="selected">
							  <?=$row->name?>
							  </option>
							  <?php else:?>
							  <option value="<?=$row->id?>" >
							  <?=$row->name?>
							  </option>
							  <?php endif;?>
							  <?php endforeach;?>	
							</select>
						</td>
					</tr>
					
					<tr>
						<td>Kab / Kota</td>
					</tr>
					<tr>
						<td> 
							<select id="kabkota" name="kabkota" >
								<option value="all">-Semua-</option>
								<?php foreach($kabkota->result() as $row):?>
								    <option value="<?=$row->id?>" <?=($row->id==$this->uri->segment(5) ? 'selected' : '')?>><?=$row->name?></option>
								<?php endforeach;?>
								
							  </select>
						</td>
					</tr>
					<tr>
						<td>Tahun</td>
					</tr>
					<tr>
						<td> 
							<select id="tahun" name="tahun" >
							<option value="all">-Semua-</option>
							<?php for($i=2014;$i<=date('Y');$i++):?>
							<?php if($this->uri->segment(2)=='graph' && $this->uri->segment(4)==$i):?>
								<option value="<?=$i?>" selected="selected"><?=$i?></option>
							<?php else:?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif;?>
							<?php endfor;?>
							</select>
						</td>
					</tr>
					<tr>
                	<td>
                    <input type="submit"  class="button_proses" value="Proses" /></td>
                </tr>
				</table>
                            
                </form>
                    
                    
               </div>  
            </div>
            <div id="tab-2" class="tab-content">
               <div class="inner">
               		
                        	<p class="color-1">Statistik Pengunjung.</p>
                          
               </div>  
            </div>
            
        </div>                        
      </div>
      </div>