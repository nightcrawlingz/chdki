<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<style>

.page1-col1, .judul_page, .container_12 .grid_8  {
    width: 1000px;
}
</style>
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/lihat_pengaduan_sms')?>" class="current">Pengaduan SMS 1771</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Pengaduan SMS 1771
			</div>
			<div style="clear: both;"></div>
	<style>
	  .article{
	  	margin: 0 0 12px;
    	padding: 0 24px;
	  }
	  .postmeta {
		background: none repeat scroll 0 0 #F8F8F8;
		border: 1px solid #EBEBEB;
		margin: 20px 15px 15px 10px;
		padding: 5px;
		}
		
	  #nav-filter{
	  	margin:0 0 12px;
		padding:0 24px 0 24px;
	  }
	  #nav-filter input[type="submit"]{
	  	margin-bottom:10px;
		
	  }
	  #nav-filter ul{
	  	width:100%;
    	
	  }
	  #nav-filter li{
	  	width:20%;
    	
	  }
	  #nav-filter .buttonHolder{
	  	padding: 0.5em;
		margin: 0.5em 0 0;
    	
	  }
	  #nav-filter .ctrlHolder{
	  	padding: 0;
		
    	
	  }
	  </style>
	  
	  <div id="nav-filter">
	  
	  
	  <form method="post" action="<?=site_url('home/post_filter/lihat_pengaduan_sms')?>" class="uniForm">
	  	 <fieldset class="inlineLabels">
	  	
	  	<div class="ctrlHolder">
		
			<ul class="alternate">
				<li>Cari
				  <input name="cari_teks" type="text" style="width: 170px; display: inline; margin-top: 7px; margin-bottom: 0px;" value="<?=$this->session->userdata('cari_teks')?>" />
				</li>
				<li>Tahun 
				  <select name="tahun">
						<option value="">-Semua-</option>
					<?php for($i=2012; $i<=date("Y"); $i++):?>
						<option value="<?=$i?>" <?=($i==$this->session->userdata('tahun')? 'selected="selected"' : '')?>><?=$i?></option>
					<?php endfor;?>
				  </select>
				</li>
				<li>
				Provinsi 
				<select name="prov" id="provinsi">
					<option value="">-Semua-</option>
					<?php foreach($prov->result() as $row):?>
					
					<option value="<?=$row->KdProv?>" <?=($row->KdProv==$this->session->userdata('provinsi')? 'selected="selected"' : '')?>> 
					<?=$row->NmProv?> 
					</option>
					
					<?php endforeach;?>
				  </select>
				</li>
				<li>
				Kabkota 
				<select id="kabkota" name="kabkota" >
				<option value="">-Semua-</option>
				<?php foreach($kabkota->result() as $row):?>
					<option value="<?=$row->KdKab?>" <?=($row->KdKab==$this->session->userdata('kabkota')? 'selected="selected"' : '')?>>
					  <?=$row->NmKab?>
					</option>
				<?php endforeach;?>
				</select> 
				</li>
			</ul>
		</div>
		 </fieldset>
		
		
		
		<div class="buttonHolder">
        	
			<a href="<?=site_url('home/post_filter_hapus/lihat_pengaduan')?>" class="primaryAction" style="padding-top: 7px; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; padding-bottom: 9px; margin:0">Hapus</a>
        	<button class="primaryAction" type="submit">Filter</button>
      	</div>
		</form>
	  </div>
	  <?php foreach($pengaduan->result() as $row):?>        
	  <div class="article" >

          <div class="clr"></div>
          <p class="infopost" >Di Posting <?=mysqldatetime_to_date_id($row->tanggal)?> Oleh <?=substr_replace($row->msisdn, 'xxx', -3, 3)?>, <?=$row->alamat?><br /><?=$row->nama_sekolah?>, <?=$row->kabkota?>, <?=$row->prov?></p>
			
			<p><?=$row->deskripsi?></p>
		  
		  
      </div>
	  <?php
	  	//echo $row->kdpengaduan;
	  	$reply=$this->Select_db->reply_pengaduan($row->kdpengaduan);
		if($reply->num_rows()!=0):
	  ?>
	  <div class="article">
          
          <div class="clr"></div>
		  <?php foreach($reply->result() as $isi):?>
          <div class="comment" style="background: none repeat scroll 0 0 #F8F8F8;border-color: #F0F0F0;
    border-style: solid;
    border-width: 1px 1px 0;
    margin: 12px 10px;
    padding: 0;"> <p><div style="float:left"><a href="#"><img class="userpic" alt="" src="<?=$this->config->item('home_img')?>/logo_mini.png"></a></div>
            <a href="#" >Tim BSM</a> Menjawab:<br>
              <?=mysqldatetime_to_date_id($isi->TglUpdate)?></p>
            <p><?=$isi->Uraian?></p>
          </div>
		  <?php endforeach;?>
        </div>
		<?php endif;?>
		<p class="postmeta"></p>
	  <?php endforeach;?>
      
		<div style="clear: both;"></div>
		<?php include('inc/share_button.php');?>
	
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
	  <!-- sidebar end -->
<div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
	$(function() {
			var searchTerm = jQuery.parseJSON('<?=json_encode(explode(' ',$this->session->userdata('cari_teks')))?>');
			var cek = '<?=$this->session->userdata('cari_teks')?>';
			// remove any old highlighted terms
			$('body').removeHighlight();
	
			// disable highlighting if empty
			if(cek){
				length = searchTerm.length;
				for (var i = 0; i < length; i++) {
				  $('.article').highlight( searchTerm[i] );
				}
			}
	});
</script>
</body>
</html>