<?php
$uri2=$this->uri->segment(2);
$uri3=$this->uri->segment(3);
$uri4=$this->uri->segment(4);
$admin = $this->session->userdata('admin');
?>
<div id="left">
        <div class="media user-media">
          <a class="user-link" href="">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?=$this->config->item('admin_handling_img')?>/user.png">
            <?php
		$query = $this->pengaduan_db->get_all(array('approved' => 0));
		$num_unapproved = $query->num_rows();
		
		if($num_unapproved):
            ?>
            <span class="label label-danger user-label"><?=$num_unapproved?></span>
	    <?php endif;
	    ?>
          </a>
          <div class="media-body">
            <h5 class="media-heading"><?=$admin->nama?></h5>
            <ul class="list-unstyled user-info">
              <li> <a href="">Administrator</a> </li>
              <li>Terakhir Login :
                <br>
                <small>
                  <i class="fa fa-calendar"></i>&nbsp;<time class="" datetime="<?=date('c',$admin->last_login)?>"><?=time_ago($admin->last_login)?></time>
		</small>
              </li>
            </ul>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="collapse">
          <li class="nav-header">Menu</li>
          <li class="nav-divider"></li>
          <li <?=($uri2=='pengaduan' ? 'class="active"' : '')?>>
            <a href="javascript:;">
              <i class="glyphicon-th-list"></i>
              <span class="link-title">Pengaduan</span>
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li <?=($uri2=='pengaduan' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/pengaduan')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Pengaduan Online
                </a>
              </li>
            </ul>
          </li>
          <li <?=($uri2=='halaman' ? 'class="active"' : '')?>>
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>&nbsp;Halaman
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li <?=($uri2=='halaman' && $uri3=='tentang' && $uri4=='bop' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/tentang/bop')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Tentang BOP</a>
              </li>
              <li <?=($uri2=='halaman' && $uri3=='tentang' && $uri4=='kjp' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/tentang/kjp')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Tentang KJP</a>
              </li>
              <li <?=($uri2=='halaman' && $uri3=='berita' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/berita')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Berita</a>
              </li>
              <li <?=($uri2=='halaman' && $uri3=='pengumuman' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/pengumuman')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Pengumuman</a>
              </li>
              <li <?=($uri2=='halaman' && $uri3=='peraturan' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/peraturan')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Peraturan Perundangan</a>
              </li>
              <li <?=($uri2=='halaman' && $uri3=='publikasi' ? 'class="active"' : '')?>>
                <a href="<?=site_url('admin_handling/halaman/publikasi')?>">
                  <i class="fa fa-angle-right"></i>&nbsp;Publikasi</a>
              </li>
            </ul>
          </li>
          <li <?=($uri2=='user_handling' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/user_handling')?>">
              <i class="fa fa-table"></i>&nbsp; User Handling</a>
          </li>
          <li <?=($uri2=='file_manager' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/file_manager')?>">
              <i class="fa fa-file"></i>&nbsp;File Manager</a>
          </li>
          
        </ul><!-- /#menu -->
      </div><!-- /#left -->