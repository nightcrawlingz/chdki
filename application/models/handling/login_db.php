<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function login_handling($data=null){
        $this->db->select('a.id, a.username, a.last_login, b.nama, b.nip, b.jabatan_dinas, b.jabatan_kjp_bop, b.email, b.alamat_kantor, b.telp_kantor, b.fax_kantor, b.alamat_rumah, b.handphone, a.person_id');
        $this->db->from('user_handling a')
                ->join('person b','a.person_id = b.id','left')
                ->where('a.username',$data['username'])
                ->where('a.password',md5($data['password']))
                ->where('a.activated',1)
                ->where('a.deleted',0);
        $is_login=$this->db->get();
        if($is_login->num_rows()==1){
            $result = $is_login->row();
	    
	    $time = time();
	    $ip = $this->input->ip_address();
	    $this->db->where('id',$result->id);
            $this->db->update('user_handling', array('last_login' => $time, 'last_ip' => $ip));
            
            //$result->last_login = $time;
            //$result->last_ip = $ip;
            return $result;
        }
        else
            return false;
    }  
}
?>