<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Respon_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function exist($id){
        return $this->db->get_where('respon',  array('id' => $id));
    }
    function save($id=null, $data_respon, $data_pengaduan=array()){
	$result=false;
        $CI =& get_instance();
	$CI->load->model('admin_handling/pengaduan_db');
	
	$this->db->trans_start();
	
	$exist=$this->exist($id);
	if($exist->num_rows() == 1){
	    //update
	    $respon = $exist->row();
	    
	    //save respon
	    $this->db->where('id', $id);
	    if($result = $this->db->update('respon', $data_respon)){
		//update pengaduan
		if($data_pengaduan){
		    $pengaduan_id = $respon->pengaduan_id;
		    $result = $CI->pengaduan_db->save_pengaduan($pengaduan_id, $data_pengaduan);
		}
		
	    }
	}else{
	    //insert
	    
	    //save respon
	    if($result = $this->db->insert('respon', $data_respon)){
		//update pengaduan
		if($data_pengaduan){
		    $pengaduan_id = $data_respon['pengaduan_id'];
		    $result = $CI->pengaduan_db->save_pengaduan($pengaduan_id, $data_pengaduan);
		}
		
	    }
	}
	
	$this->db->trans_complete();
	
	//echo $this->db->last_query();
	return $result;
    }
    
    
    function get($id){
	return $this->get_all(array('id' => $id));
    }
    
    function get_all($filter=array()){
	$this->db->select('a.id, a.tanggal, a.deskripsi, e.nama as user, c.name as level, f.name as status, a.penyelesaian_id, a.pengaduan_id, d.username, a.user_handling_id');
	
	$this->db->from('respon a');
	$this->db->join('role_level b','a.user_handling_id = b.user_id','left');
	$this->db->join('level c', 'b.level_id = c.id', 'left');
	$this->db->join('user_handling d', 'd.id = a.user_handling_id', 'left');
	$this->db->join('person e', 'e.id = d.person_id', 'left');
	$this->db->join('status f','f.id = a.status_id', 'left');
	if(isset($filter['pengaduan_id']))
	  $this->db->where('a.pengaduan_id', $filter['pengaduan_id']);
	
	if(isset($filter['id']))
	  $this->db->where('a.id', $filter['id']);
	
	$this->db->where('a.deleted',0);
	return $this->db->get();
    }
    
    function delete($id){
	$status_id = 1;
	$respon = $this->get($id)->row();
	$this->db->where('pengaduan_id', $respon->pengaduan_id);
	$this->db->where_not_in('id', array($id));
	$this->db->order_by('last_update', 'desc');
	$this->db->where('deleted',0);
	$this->db->limit(1);
	$query = $this->db->get('respon');
	
	if($query->num_rows() == 1){
	    $last_respon = $query->row();
	    $status_id = $last_respon->status_id;
	}
	return $this->save($id, array('deleted' => 1), array('status_id' => $status_id));
    }
}
?>